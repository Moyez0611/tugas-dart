import 'dart:async';

void main() async {
  var h = Human();

  print("Luffy");
  print("Zoro");
  print("Killer");
  print(h.name);
  await h.getData();
  print(h.name);
}

class Human {
  String name = "Nama character one piece";
  Future<void> getData() async {
    await Future.delayed(Duration(seconds: 3));
    name = "Hilmy";
    print("get data[done]");
  }
}
