import 'bangun_datar.dart';

class lingkaran extends bangun_datar {
  late double a;

  lingkaran(double a) {
    this.a = a;
  }

  @override
  double luas() {
    return 3.14 * a * a;
  }

  @override
  double keliling() {
    return 2 * 3.14 * a;
  }
}
