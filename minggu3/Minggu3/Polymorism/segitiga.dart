import 'bangun_datar.dart';

class segitiga extends bangun_datar {
  late double a;
  late double b;
  late double c;

  segitiga(double a, double b, double c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  @override
  double luas() {
    return 0.5 * a * b;
  }

  @override
  double keliling() {
    return a + b + c;
  }
}
