import 'bangun_datar.dart';
import 'segitiga.dart';
import 'lingkaran.dart';
import 'persegi.dart';

void main() {
  bangun_datar operasi = new bangun_datar();
  lingkaran Lingkaran = new lingkaran(14);
  persegi Persegi = new persegi(4);
  segitiga Segitiga = new segitiga(12, 5, 13);

  operasi.luas();
  print("Luas Lingkaran: ${Lingkaran.luas()}");
  print("Luas Persegi: ${Persegi.luas()}");
  print("Luas Segitiga: ${Segitiga.luas()}");

  operasi.keliling();
  print("Keliling Lingkaran: ${Lingkaran.keliling()}");
  print("Keliling Persegi: ${Persegi.keliling()}");
  print("Keliling Segitiga: ${Segitiga.keliling()}");
}
