class Lingkaran {
  //tulis coding disini
  late double _phi;
  late double _r;

  void set phi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _phi = value;
  }

  double get phi {
    return _phi;
  }

  void set r(double value) {
    if (value < 0) {
      value *= -1;
    }
    _r = value;
  }

  double get r {
    return _r;
  }

  double get luas => _phi * _r * _r;
}
