//Function sederhana tanpa return
void main() {
  tampilkan();
}

tampilkan() {
  print("Hello World");
}

// //Function sederhana dengan return
// void main() {
//   print(munculkan_angka());
// }

// munculkan_angka() {
//   return 2;
// }

// //Function dengan parameter
// void main() {
//   print(kalikan_dua(6));
// }

// kalikan_dua(angka) {
//   return angka * 2;
// }

// //Function Pengiriman parameter lebih dari satu
// void main() {
//   print(kalikan(5, 6));
// }

// kalikan(x, y) {
//   return x * y;
// }

// //Function Inisialisasi parameter dengan nilai default
// void main() {
//   tampilkan_angka(5);
// }

// tampilkan_angka(n1, {s1: 45}) {
//   print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
//   print(s1); //hasil adalah 35 karena dari parameter diisi 45
// }

// //Pengiriman parameter yang UNDEFINED!
// void main() {
//   print(function_Perkalian(5, 6));
// }

// function_Perkalian(angka1, angka2) {
//   return angka1 * angka2;
// }
