import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main() {
  armor_titan art = armor_titan();
  attack_titan at = attack_titan();
  beast_titan bt = beast_titan();
  human h = human();

  art.powerPoint = 10;
  at.powerPoint = 5;
  bt.powerPoint = 20;
  h.powerPoint = 100;

  print("Power Point Armor Titan = ${art.powerPoint}");
  print("Power Point Attack Titan = ${at.powerPoint}");
  print("Power Point Beast Titan = ${bt.powerPoint}");
  print("Power Point Armor Titan = ${h.powerPoint}");

  print("Suara dari Armor Titan: " + art.terjang());
  print("Suara dari Attack Titan: " + at.punch());
  print("Suara dari Beast Titan: " + bt.lempar());
  print("Suara dari Human: " + h.killAlltitan());
}
