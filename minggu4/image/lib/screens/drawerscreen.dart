import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const UserAccountsDrawerHeader(
            accountName: Text('Moyez'),
            accountEmail: Text('moya.abs2@gmail.com'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/images/dmaria.jpg'),
            ),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.group),
            title: const Text('New Group'),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.lock),
            title: const Text('New Secret Group'),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.notifications),
            title: const Text('New Channel Chat'),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.contacts),
            title: const Text('Contacts'),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.bookmark_border),
            title: const Text('Saved Message'),
          ),
          ListTile(
            dense: true,
            onTap: () {},
            leading: const Icon(Icons.phone),
            title: const Text('Calls'),
          ),
        ],
      ),
    );
  }
}
