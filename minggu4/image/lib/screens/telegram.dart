import 'package:flutter/material.dart';
import 'package:image/models/Chart_model.dart';
import 'package:image/screens/drawerscreen.dart';

class Telegram extends StatelessWidget {
  const Telegram({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Telegram'),
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: const CustomDrawer(),
      body: ListView.separated(
        itemCount: items.length,
        itemBuilder: (context, index) {
          final profile = items[index];

          return ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(profile.profileUrl!),
            ),
            title: Text(
              profile.name!,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(profile.message!),
            trailing: Text(profile.time!),
          );
        },
        separatorBuilder: (context, index) => const Divider(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(
          Icons.create,
          color: Colors.white,
        ),
      ),
    );
  }
}
